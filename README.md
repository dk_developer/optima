# Optima

Simple showcase for the 8 task queues of
 Petrov-Sokolitsin's and Johnson's methods

------------------------------------------

Installation
------------------------------------------
You will need conda 
(https://docs.conda.io/en/latest/)
to install environment (the easiest way).

`conda env create -f environment.yml`

(More info on installing conda environments
 here -- https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file)

Usage
------------------------------------------

In order to view the queues simply run 
`functions.py` file using conda environment.
