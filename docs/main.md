---
figureTitle: Рисунок
tableTitle: Таблица
tableEqns: true
titleDelim: "&nbsp;–"
link-citations: true
linkReferences: true
chapters: true
...

# [Содержание]{custom-style="UnnumberedHeadingOneNoTOC"} {.unnumbered}

%TOC%

# Описание методов {#sec:chapter1}

Для эффективной реализации методов решения задачи календарного планирования требуется понимать сами методы.
В общем случае они сводятся к созданию очередей обработки M деталей на N станках. Основываясь на различных
критериях, эти очереди могут являть собой эффективные (каждая - в своем роде) решения задачи.

## Метода Петрова – Соколицына

У данного метода существуют три вариации очереди.

### Первая очередь

В обработку сначала поступают детали с суммарно наибольшим временем обработки на всех станках кроме первого.

### Вторая очередь

В обработку сначала поступают детали с суммарно наименьшим временем обработки на всех станках кроме последнего.

### Третья очередь

В обработку сначала поступают детали с наибольшей разницей показателей второй и третьей очереди.

## Обобщения Джонсона

Алгоритм обобщений Джонсона также включает в себя несколько возможных очередей, каждая из которых в своем
род эффективно решает поставленную задачу.

### Первая очередь

В обработку сначала поступают детали с минимальной продолжительностью обработки на первом станке.
Таким образом удается максимально сократить простой второго станка.

### Вторая очередь

В обработку сначала поступают детали с максимальной продолжительностью обработки на последнем станке.
Подобная очередность позволяет максимально сократить простой последнего станка.

### Третья очередь

В обработку сначала подаются детали с максимальным индексом "узкого места". "Узким местом" называется 
максимальное время работы с деталью из всех существующих продолжительностей обработки этой детали на
станках.

### Четвертая очередь

В обработку сначала поступают детали с максимальным суммарным временем обработки на всех станках.

### Пятая очередь

Реализует попытку объединить вышеизложенные характеристики. Сортировка происходит в порядке возрастания
суммы индексов данной детали в других очередях.

## Диаграммы Ганта

Диаграммы Ганта - графический инструмент для удобного создания, изменения и просмотра планов проектной
деятельности. 

![Пример небольшой диаграммы Ганта](images/gantt.gif){#fig:figure1 width=390px}

Как видно на Рисунке [-@fig:figure1], диаграмма состоит из нескольких частей:

- оси X и отметок времени на ней;
- оси Y и отметок задач на ней;
- прямоугольников, иллюстрирующих продолжительность выполнения задачи.

Используя подобную форму визуализации информации можно существенно повысить производительность персонала,
занимающегося планированием или исполнением планов.

# Разработка программы {#sec:chapter2}

В рамках данной работы была разработана программная реализация обобщений Джонсона
и метода Петрова-Соколицына. Тестовые данные для программы создаются на основе генератора псевдослучайных
чисел. Результатом работы компонента является серия масштабируемых диаграмм Ганта. Основные параметры (
такие как количество деталей или машин) можно задать непосредственно в коде программы.

## Диаграмма состояний

Несмотря на то, что программа полностью линейна, диаграмма состояний позволит лучше понять
последовательность действий компонента, представив ее графически.

![Диаграмма состояний программы](images/state_diagram.png){#fig:figure2}

## Диаграмма последовательности

![Диаграмма последовательностей программы](images/sequence_diagram.png){#fig:sequence_diagram}

Диаграмма на Рисунке [-@fig:sequence_diagram] наглядно иллюстрирует процессы протекающие внутри решения.

## Реализация алгоритмов и диаграммы

В качестве языка программирования для разработки программы был выбран Python. Реализация вычислений
производилась с использованием библиотеки Pandas (структуры данных) и MatPlotLib (для изображения 
диаграмм Ганта).

### Функция генерации данных

Для того, чтобы продемонстрировать пользователю работу модуля, избавив его от необходимости
предоставлять данные, была разработана функция генерации тестовых данных. Ниже приведен ее код
на языке Python.

~~~ {.python}
def get_random_tasks(tasks: int, machines: int, min_bound: int, max_bound: int) -> pd.DataFrame:
    """
    Return pandas' DataFrame based on parameters given

    :param tasks: number of tasks to queue
    :param machines: number of available machines
    :param min_bound: minimum bound of time spend doing some task on some machine
    :param max_bound: maximum bound of time spend doing some task on some machine
    :return: pandas.DataFrame containing TASKSxMACHINES durations matrix
    """
    return pd.DataFrame.from_records(
        [{'durations': [random.randint(min_bound, max_bound) for j in range(machines)]} for i in range(tasks)]
    )
~~~

### Функция точек для диаграммы Ганта

Для получения массива точек диаграммы Ганта была разработана функция,
листинг которой вы можете видеть ниже.

~~~ {.python}
def get_graph_df(df: pd.DataFrame) -> pd.DataFrame:
    """
    Return pandas.DataFrame, which can be plotted using matplotib's broken_barh.

    :param df: Sorted pandas.DataFrame of Tasks
    :return: pandas.DataFrame with additional columns: "graph_x" and "graph_y"
    """
    index = []
    end_times = []
    for i, durations in enumerate(df.unstack().iteritems()):
        if durations[0][0] == 'durations':
            times = []
            graph_x = []
            graph_y = []
            for j, x in enumerate(durations[1]):
                one = (end_times[i - 1]['end_times'][j] if i > 0 else 0)
                two = (times[j - 1] if j > 0 else 0)
                start_time = max(one, two)
                times.append(start_time + x)
                graph_x.append((start_time, x))
                graph_y.append((j + 1, 1))
            end_times.append({"end_times": times, "graph_x": graph_x, "graph_y": graph_y})
            index.append(durations[0][1])
    records = pd.DataFrame.from_records(end_times, index=index)
    results = df.join(records)
    return results.drop(columns=[
        'ps1',
        'ps2',
        'ps3',
        'oj1',
        'oj2',
        'oj3',
        'oj4',
        'durations',
        'end_times'
    ])
~~~

### Реализация методов сортировки

Для целей программы была выбрана следующая реализация функций, на основе которых строились очереди 
задач:

~~~ {.python}
# Метод Петрова-Соколицына
ps1 = lambda x: -sum(x['durations'][1:])
ps2 = lambda x: sum(x['durations'][:-1])
ps3 = lambda x: -(sum(x['durations'][:-1]) - sum(x['durations'][1:]))

# Метод обобщений Джонсона
oj1 = lambda x: x['durations'][0]
oj2 = lambda x: -x['durations'][-1]
oj3 = lambda x: -(max(reversed(list(enumerate(x['durations']))), key=lambda y: y[1])[0])
oj4 = lambda x: -(sum(x['durations']))
pre_oj5 = lambda d, x: d[x[0]]
~~~

Как видно из листинга выше, функция сортировки для пятой очереди обобщений Джонсона
пока определена не полностью. Это произошло потому, что для полного вычисления результата, необходимо
иметь информацию о результатах других сортировок.

Продолжение реализации метода будет рассмотрено в последующих подпунктах.

### Применение операций к набору данных

Полный листинг кода для работы с данными (от генерации до формирования отсортированных очередей)
представлен ниже:

~~~ {.python}
# Генерируем данные
tasks = get_random_tasks(TASKS, MACHINES, MIN_BOUND, MAX_BOUND)

# Вычисляем
tasks['ps1'] = tasks.apply(ps1, axis=1)
tasks['ps2'] = tasks.apply(ps2, axis=1)
tasks['ps3'] = tasks.apply(ps3, axis=1)
tasks['oj1'] = tasks.apply(oj1, axis=1)
tasks['oj2'] = tasks.apply(oj2, axis=1)
tasks['oj3'] = tasks.apply(oj3, axis=1)
tasks['oj4'] = tasks.apply(oj4, axis=1)

# Сортируем
tasks_end_timed = get_graph_df(tasks)
ps1_end_timed = get_graph_df(tasks.sort_values(['ps1']))
ps2_end_timed = get_graph_df(tasks.sort_values(['ps2']))
ps3_end_timed = get_graph_df(tasks.sort_values(['ps3']))
oj1_end_timed = get_graph_df(tasks.sort_values(['oj1']))
oj2_end_timed = get_graph_df(tasks.sort_values(['oj2']))
oj3_end_timed = get_graph_df(tasks.sort_values(['oj3']))
oj4_end_timed = get_graph_df(tasks.sort_values(['oj4']))
dfs = [
    tasks_end_timed,
    ps1_end_timed,
    ps2_end_timed,
    ps3_end_timed,
    oj1_end_timed,
    oj2_end_timed,
    oj3_end_timed,
    oj4_end_timed,
]

# 5-ое обобщение Джонсона
for data in dfs[1:]:
    for i, (idx, _) in enumerate(data.iterrows()):
        sums[idx] += i
oj5 = partial(pre_oj5, sums)
tasks['oj5'] = pd.Series(tasks.durations.reset_index().apply(oj5, axis=1).values, index=tasks.index)
oj5_end_timed = get_graph_df(tasks.sort_values(['oj5']))
~~~

### Отображение диаграмм Ганта

С целью понятно отобразить малое количество задач не перегружая графики лишней текстовой информацией,
было решено "раскрашивать" задачи в уникальные цвета. Листинг реализации диаграмм с помощью библиотеки
Matplotlib представлен ниже:

~~~ {.python}
colors = cm.rainbow(np.linspace(0, 1, TASKS + 1))
plt.subplots_adjust(
    top=0.97,
    bottom=0.03,
    left=0.03,
    right=0.97,
    hspace=0.2,
    wspace=0.1
)
for si, (data, title) in enumerate(zip(
        dfs + [oj5_end_timed],
        [
            "По индексу"
        ] + [
            f"Петров-Соколицын №{k + 1}" for k in range(3)
        ] + [
            f"Обощение Джонсона №{k + 1}" for k in range(5)
        ])):
    ax = plt.subplot(3, 3, si + 1)
    ax.set_title(title)
    for i, (idx, task) in enumerate(data.iterrows()):
        print(f"Working with task {i}/{TASKS} ({idx + 1})")
        for j, (xs, ys) in enumerate(zip(task.graph_x, task.graph_y)):
            plt.broken_barh([xs], ys, color=colors[idx], axes=ax)
plt.show()
~~~

## Руководство пользователя

Для начала демонстрации запустите исполняемый файл.

![Возможный вид окна программы](images/screen.png){#fig:figure3}

После запуска программа будет выглядеть как на Рисунке [-@fig:figure3] (с точностью до графиков).

В целом, интерфейс интуитивно понятен. Для масштабирования каждого графика, пользователь должен нажать
на иконку с лупой и выделить левой кнопкой мыши интересующую область изображения для увеличения масштаба.
Для уменьшения масштаба используется выделение правой кнопкой мыши. Для возврата к исходному состоянию
присутствует клавиша с иконкой домика.

# Заключение
В рамках разработки модуля планирования MES-системы была реализована
программа на языке Python с использованием компонентов Pandas и Matplotlib. Решение демонстрирует работу
алгоритмов обобщений Джонсона и метода Петрова-Соколицына. В качестве исходных данных используется 
матрица псевдослучайных чисел. 
Результатом работы программы являются диаграммы Ганта для каждого из методов.