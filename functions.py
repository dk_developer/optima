import datetime
import os
import random
from collections import defaultdict
from functools import partial
from pathlib import Path
from typing import List

import numpy as np
import pandas as pd
import matplotlib
from matplotlib import cm

MAX_BOUND = 100

MIN_BOUND = 10

MACHINES = 10

TASKS = 10

matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt


def get_wasted_time(graph_df: pd.DataFrame) -> int:
    """
    Returns the wasted time of the system run

    :param graph_df: pandas.DataFrame from get_graph_df function
    :return: Whole time wasted
    """
    times = [
        [] for i in range(graph_df['graph_y'][0][-1][0])
    ]
    for idx, (_, row) in enumerate(graph_df['graph_x'].iteritems()):
        if idx < len(graph_df) - 1:
            for i, (_, t) in enumerate(row):
                times[i].append(t)
        else:
            return sum([t - sum(times[i]) for i, (t, _) in enumerate(row)])


def get_random_tasks(tasks: int, machines: int, min_bound: int, max_bound: int) -> pd.DataFrame:
    """
    Return pandas.DataFrame based on parameters given

    :param tasks: number of tasks to queue
    :param machines: number of available machines
    :param min_bound: minimum bound of time spend doing some task on some machine
    :param max_bound: maximum bound of time spend doing some task on some machine
    :return: pandas.DataFrame containing TASKSxMACHINES durations matrix
    """
    return pd.DataFrame.from_records(
        [{'durations': [random.randint(min_bound, max_bound) for j in range(machines)]} for i in range(tasks)]
    )


def get_graph_df(df: pd.DataFrame) -> pd.DataFrame:
    """
    Return pandas.DataFrame, which can be plotted using matplotib's broken_barh.

    :param df: Sorted pandas.DataFrame of Tasks
    :return: pandas.DataFrame with additional columns: "graph_x" and "graph_y"
    """
    index = []
    end_times = []
    for i, durations in enumerate(df.unstack().iteritems()):
        if durations[0][0] == 'durations':
            times = []
            graph_x = []
            graph_y = []
            for j, x in enumerate(durations[1]):
                one = (end_times[i - 1]['end_times'][j] if i > 0 else 0)
                two = (times[j - 1] if j > 0 else 0)
                start_time = max(one, two)
                times.append(start_time + x)
                graph_x.append((start_time, x))
                graph_y.append((j + 1, 1))
            end_times.append({"end_times": times, "graph_x": graph_x, "graph_y": graph_y})
            index.append(durations[0][1])
    records = pd.DataFrame.from_records(end_times, index=index)
    results = df.join(records)
    return results.drop(columns=[
        'ps1',
        'ps2',
        'ps3',
        'oj1',
        'oj2',
        'oj3',
        'oj4',
        'durations',
        'end_times'
    ])


if __name__ == '__main__':
    sums = defaultdict(lambda: 0)

    # Метод Петрова-Соколицына
    ps1 = lambda x: -sum(x['durations'][1:])
    ps2 = lambda x: sum(x['durations'][:-1])
    ps3 = lambda x: -(sum(x['durations'][:-1]) - sum(x['durations'][1:]))

    # Метод обобщений Джонсона
    oj1 = lambda x: x['durations'][0]
    oj2 = lambda x: -x['durations'][-1]
    oj3 = lambda x: -(max(reversed(list(enumerate(x['durations']))), key=lambda y: y[1])[0])
    oj4 = lambda x: -(sum(x['durations']))
    pre_oj5 = lambda d, x: d[x[0]]

    # Генерируем данные
    tasks = get_random_tasks(TASKS, MACHINES, MIN_BOUND, MAX_BOUND)

    # Вычисляем
    tasks['ps1'] = tasks.apply(ps1, axis=1)
    tasks['ps2'] = tasks.apply(ps2, axis=1)
    tasks['ps3'] = tasks.apply(ps3, axis=1)
    tasks['oj1'] = tasks.apply(oj1, axis=1)
    tasks['oj2'] = tasks.apply(oj2, axis=1)
    tasks['oj3'] = tasks.apply(oj3, axis=1)
    tasks['oj4'] = tasks.apply(oj4, axis=1)

    # Сортируем
    tasks_end_timed = get_graph_df(tasks)
    ps1_end_timed = get_graph_df(tasks.sort_values(['ps1']))
    ps2_end_timed = get_graph_df(tasks.sort_values(['ps2']))
    ps3_end_timed = get_graph_df(tasks.sort_values(['ps3']))
    oj1_end_timed = get_graph_df(tasks.sort_values(['oj1']))
    oj2_end_timed = get_graph_df(tasks.sort_values(['oj2']))
    oj3_end_timed = get_graph_df(tasks.sort_values(['oj3']))
    oj4_end_timed = get_graph_df(tasks.sort_values(['oj4']))
    dfs = [
        tasks_end_timed,
        ps1_end_timed,
        ps2_end_timed,
        ps3_end_timed,
        oj1_end_timed,
        oj2_end_timed,
        oj3_end_timed,
        oj4_end_timed,
    ]

    # 5-ое обобщение Джонсона
    for data in dfs[1:]:
        for i, (idx, _) in enumerate(data.iterrows()):
            sums[idx] += i
    oj5 = partial(pre_oj5, sums)
    tasks['oj5'] = pd.Series(tasks.durations.reset_index().apply(oj5, axis=1).values, index=tasks.index)
    oj5_end_timed = get_graph_df(tasks.sort_values(['oj5']))
    dfs += [oj5_end_timed]

    tasks_wasted_time = get_wasted_time(tasks_end_timed)
    ps1_wasted_time = get_wasted_time(ps1_end_timed)
    ps2_wasted_time = get_wasted_time(ps2_end_timed)
    ps3_wasted_time = get_wasted_time(ps3_end_timed)
    oj1_wasted_time = get_wasted_time(oj1_end_timed)
    oj2_wasted_time = get_wasted_time(oj2_end_timed)
    oj3_wasted_time = get_wasted_time(oj3_end_timed)
    oj4_wasted_time = get_wasted_time(oj4_end_timed)
    oj5_wasted_time = get_wasted_time(oj5_end_timed)

    t_str = str(datetime.datetime.now().timestamp())

    for df, prefix in zip([
        tasks,
        tasks.sort_values(['ps1']),
        tasks.sort_values(['ps2']),
        tasks.sort_values(['ps3']),
        tasks.sort_values(['oj1']),
        tasks.sort_values(['oj2']),
        tasks.sort_values(['oj3']),
        tasks.sort_values(['oj4']),
    ], [
        "original_{time}.csv",
        "ps1_{time}.csv",
        "ps2_{time}.csv",
        "ps3_{time}.csv",
        "oj1_{time}.csv",
        "oj2_{time}.csv",
        "oj3_{time}.csv",
        "oj4_{time}.csv",
        "oj5_{time}.csv",
    ]):
        if not os.path.exists('output'):
            os.mkdir('output')
        with open(os.path.join('output', prefix.format(time=t_str)), 'w') as file:
            file.write(df.to_csv())

    colors = cm.rainbow(np.linspace(0, 1, TASKS + 1))
    plt.subplots_adjust(
        top=0.97,
        bottom=0.03,
        left=0.03,
        right=0.97,
        hspace=0.2,
        wspace=0.1
    )
    for si, (data, title, wasted_time) in enumerate(zip(
            dfs,
            [
                "По индексу"
            ] + [
                f"Петров-Соколицын №{k + 1} | Простой: " for k in range(3)
            ] + [
                f"Обощение Джонсона №{k + 1} | Простой: " for k in range(5)
            ],
            (
                    tasks_wasted_time,
                    ps1_wasted_time,
                    ps2_wasted_time,
                    ps3_wasted_time,
                    oj1_wasted_time,
                    oj2_wasted_time,
                    oj3_wasted_time,
                    oj4_wasted_time,
                    oj5_wasted_time
            ))):
        ax = plt.subplot(3, 3, si + 1)
        ax.set_title(title + str(wasted_time) if si > 0 else title)
        for i, (idx, task) in enumerate(data.iterrows()):
            print(f"Working with task {i}/{TASKS} ({idx + 1})")
            for j, (xs, ys) in enumerate(zip(task.graph_x, task.graph_y)):
                plt.broken_barh([xs], ys, color=colors[idx], axes=ax)
    plt.show()
